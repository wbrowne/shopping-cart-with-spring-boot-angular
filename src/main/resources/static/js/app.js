var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/products', {
                templateUrl: 'products.html',
                controller: 'ProductsCtrl'
            }).
            when('/cart', {
                templateUrl: 'cart.html',
                controller: 'CartCtrl'
            }).
            when('/orders', {
                templateUrl: 'orders.html',
                controller: 'OrderCtrl'
            }).
            otherwise({
                redirectTo: '/products'
            });
    }]);

app.factory("productFactory", function ($http) {
    var factory = {};

    factory.getProductList = function () {
        return $http.get("/products");
    };

    return factory;
});

app.factory("cartFactory", function ($http) {
    var factory = {};

    factory.addToCart = function (product, quantity) {
        var data = {};
        data.product = {};
        data.product.id = product.id;
        data.quantity = quantity;
        return $http.post("/cart", data);
    };

    factory.getCart = function () {
        return $http.get("/cart");
    };

    factory.getCartCount = function () {
        return $http.get("/cartCount");
    };

    factory.updateCart = function (productOrder) {
        var data = {};
        data.product = {};
        data.product.id = productOrder.product.id;
        data.quantity = productOrder.quantity;

        return $http.post("/updateCart", data);
    };

    factory.removeFromCart = function (productOrder) {
        var data = {};
        data.product = {};
        data.product.id = productOrder.product.id;

        return $http.post("/deleteCart", data);
    };

    return factory;
});

app.factory("orderFactory", function ($http) {
    var factory = {};

    factory.submitOrder = function () {
        return $http.post("/orders");
    };

    factory.getOrders = function () {
        return $http.get("/orders");
    };

    return factory;
});

app.controller("OrderCtrl", function ($scope, orderFactory) {

    function init() {
        $scope.errormessage = "";

        orderFactory.getOrders().success(function (data) {
            $scope.orders = data;
        }).error(function (data, status, headers, config) {
            $scope.setErrorMessage("Failed to fetch orders");
        });

        $scope.setErrorMessage = function (message) {
            $scope.errormessage = message;
        };
    }

    init();
});

app.controller("ProductsCtrl", function ($scope, productFactory, cartFactory) {
    function init() {
        $scope.statusmessage = "";
        $scope.errormessage = "";

        productFactory.getProductList().success(function (data) {
            $scope.products = data;
        });

        $scope.addToCart = function (product, quantity) {
            cartFactory.addToCart(product, quantity).success(function (data) {
                $scope.updateCartCount();
                $scope.setStatusMessage("Added (" + quantity + ") " + product.name + " to Cart");
            }).error(function (data, status, headers, config) {
                $scope.setErrorMessage("Failed to add (" + quantity + ") " + product.name + " to the Cart");
            });
        };

        $scope.updateCartCount = function () {
            cartFactory.getCartCount().success(function (data) {
                $scope.cartCount = data;
            });
        };

        $scope.setStatusMessage = function (message) {
            $scope.statusmessage = message;
            $scope.errormessage = '';
        };

        $scope.setErrorMessage = function (message) {
            $scope.errormessage = message;
            $scope.statusmessage = '';
        };

        $scope.updateCartCount();
    }

    init();
});

app.controller("CartCtrl", function ($scope, cartFactory, orderFactory) {
    function init() {
        $scope.statusmessage = "";
        $scope.errormessage = "";
        cartFactory.getCart().success(function (data) {
            $scope.productOrders = data;
        });
        $scope.updateCartCount();
    }

    $scope.updateCartCount = function () {
        cartFactory.getCartCount().success(function (data) {
            $scope.cartCount = data;
        });
    };

    $scope.updateCart = function (productOrder) {
        cartFactory.updateCart(productOrder).success(function (data) {
            $scope.productOrders = data;
            $scope.updateCartCount();
            $scope.setStatusMessage("Updated order for product " + productOrder.product.name);
        }).error(function (data, status, headers, config) {
            $scope.setErrorMessage("Failed to update order for " + productOrder.product.name);
        });
    };

    $scope.removeFromCart = function (productOrder) {
        cartFactory.removeFromCart(productOrder).success(function (data) {
            $scope.productOrders = data;
            $scope.updateCartCount();
            $scope.setStatusMessage("Deleted order for product " + productOrder.product.name);
        }).error(function (data, status, headers, config) {
            $scope.setErrorMessage("Failed to delete " + productOrder.product.name + " from the Cart");
        });
    };

    $scope.submitOrder = function () {
        orderFactory.submitOrder().success(function (data) {
            $scope.productOrders = data;
            $scope.updateCartCount();
            $scope.setStatusMessage("Your order has been submitted");
        }).error(function (data, status, headers, config) {
            $scope.setErrorMessage("Failed to submit your order");
        });
    };

    $scope.setErrorMessage = function (message) {
        $scope.errormessage = message;
        $scope.statusmessage = '';
    };

    $scope.setStatusMessage = function (message) {
        $scope.statusmessage = message;
        $scope.errormessage = '';
    };

    init();
});