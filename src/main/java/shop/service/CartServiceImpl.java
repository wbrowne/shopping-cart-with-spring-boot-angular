package shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shop.domain.Cart;
import shop.domain.Order;
import shop.domain.ProductOrder;

@Service
public class CartServiceImpl implements CartService {

  private Cart cart;

  public CartServiceImpl() {
  }

  @Autowired
  public void setCart(Cart cart) {
    this.cart = cart;
  }

  @Override
  public ProductOrder addToCart(ProductOrder productOrder) {
    return cart.addToCart(productOrder);
  }

  @Override
  public void removeFromCart(String productId) {
    cart.removeItemFromCart(productId);
  }

  @Override
  public void updateCart(ProductOrder productOrder) {
    cart.updateCart(productOrder);
  }

  @Override
  public List<ProductOrder> getProductsInCart() {
    return cart.getProductsInCart();
  }

  @Override
  public int getNumberOfProductsInCart() {
    return cart.getNumProductsInCart();
  }

  @Override
  public Order submitOrder() {

    List<ProductOrder> productOrders = getProductsInCart();

    if (productOrders.isEmpty()) {
      return null;
    }

    Order order = new Order(productOrders);

    clearCart();

    return order;
  }

  private void clearCart() {
    cart.clearCart();
  }
}
