package shop.service;

import java.util.List;

import shop.domain.Order;
import shop.domain.ProductOrder;

public interface CartService {

  ProductOrder addToCart(ProductOrder productOrder);

  List<ProductOrder> getProductsInCart();

  void removeFromCart(String productId);

  void updateCart(ProductOrder productOrder);

  int getNumberOfProductsInCart() ;

  Order submitOrder();
}
