package shop.controller;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import shop.domain.Order;
import shop.domain.Product;
import shop.domain.ProductOrder;
import shop.service.CartService;

@RestController
public class CartController {

  private MongoTemplate mongoTemplate;

  private CartService cartService;

  @Autowired
  public CartController(CartService cartService, MongoTemplate mongoTemplate) {
    this.cartService = cartService;
    this.mongoTemplate = mongoTemplate;
  }

  @RequestMapping(value = "/cart", method = RequestMethod.GET)
  public List<ProductOrder> listProductOrders() {
    return cartService.getProductsInCart();
  }

  @RequestMapping(value = "/cart", method = RequestMethod.POST)
  public ProductOrder addToCart(@RequestBody ProductOrder productOrder) {
    Product product = getProductById(productOrder);

    return cartService.addToCart(new ProductOrder(product, productOrder.getQuantity()));
  }

  @RequestMapping(value = "/updateCart", method = RequestMethod.POST)
  public List<ProductOrder> updateCart(@RequestBody ProductOrder productOrder) {
    Product product = getProductById(productOrder);

    ProductOrder updatedProductOrder = new ProductOrder(product, productOrder.getQuantity());
    cartService.updateCart(updatedProductOrder);

    return cartService.getProductsInCart();
  }

  @RequestMapping(value = "/deleteCart", method = RequestMethod.POST)
  public List<ProductOrder> deleteFromCart(@RequestBody ProductOrder productOrder) {
    Product product = getProductById(productOrder);

    if (product != null) {
      cartService.removeFromCart(product.getId());
    }

    return cartService.getProductsInCart();
  }

  @RequestMapping(value = "/cartCount", method = RequestMethod.GET)
  public int getProductOrderCount() {
    return cartService.getNumberOfProductsInCart();
  }

  private Product getProductById(@RequestBody ProductOrder productOrder) {
    String productId = productOrder.getProduct().getId();

    return mongoTemplate.findOne(new Query(where("id").is(productId)), Product.class);
  }

  @RequestMapping(value = "/orders", method = RequestMethod.POST)
  public List<ProductOrder> submitProductOrders() {

    Order order = cartService.submitOrder();

    mongoTemplate.insert(order);

    return cartService.getProductsInCart();
  }

}