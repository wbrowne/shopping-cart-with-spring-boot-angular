package shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import shop.domain.Order;

@RestController
public class OrderController {

  @Autowired
  private MongoTemplate mongoTemplate;

  @RequestMapping("/orders")
  public List<Order> findAllOrders() {
    return mongoTemplate.findAll(Order.class);
  }

}
