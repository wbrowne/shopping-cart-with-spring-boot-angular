package shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import shop.domain.Product;

@RestController
public class ProductController {

  @Autowired
  private MongoTemplate mongoTemplate;

  @RequestMapping(value = "/products", method = RequestMethod.GET)
  public List<Product> findAllProducts() {
    return mongoTemplate.findAll(Product.class);
  }

  @RequestMapping(value = "/products", method = RequestMethod.POST)
  @ResponseStatus(HttpStatus.CREATED)
  public void createProduct(@RequestBody Product product) {
    mongoTemplate.insert(product);
  }
}
