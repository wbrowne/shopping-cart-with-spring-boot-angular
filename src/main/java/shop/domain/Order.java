package shop.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Order {

  private List<ProductOrder> productOrders;

  public Order() {
  }

  public Order(List<ProductOrder> productOrders) {
    this.productOrders = productOrders;
  }

  public List<ProductOrder> getProductOrders() {
    return productOrders;
  }

  public void setProductOrders(List<ProductOrder> productOrders) {
    this.productOrders = productOrders;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Order order = (Order) o;

    return !(productOrders != null ? !productOrders.equals(order.productOrders) : order.productOrders != null);

  }

  @Override
  public int hashCode() {
    return productOrders != null ? productOrders.hashCode() : 0;
  }
}
