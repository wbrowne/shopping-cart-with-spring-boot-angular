package shop.domain;

import java.io.Serializable;

public class ProductOrder implements Serializable {

  private Product product;

  private int quantity;

  public ProductOrder() {
  }

  public ProductOrder(Product product) {
    this.product = product;
    this.quantity = 1;
  }

  public ProductOrder(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ProductOrder that = (ProductOrder) o;

    if (quantity != that.quantity) return false;
    return !(product != null ? !product.equals(that.product) : that.product != null);

  }

  @Override
  public int hashCode() {
    int result = product != null ? product.hashCode() : 0;
    result = 31 * result + quantity;
    return result;
  }
}
