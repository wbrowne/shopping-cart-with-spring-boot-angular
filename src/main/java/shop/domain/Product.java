package shop.domain;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class Product implements Serializable {

  @Id
  private String id;

  private String name;

  private String description;

  private Money unitPrice;

  private Category category;

  private int quantity;

  public Product() {
  }

  public Product(String id, String name, String description, Money unitPrice, Category category) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.unitPrice = unitPrice;
    this.category = category;
  }

  public Product(String name, String description, Money unitPrice, Category category) {
    this.name = name;
    this.description = description;
    this.unitPrice = unitPrice;
    this.category = category;
  }

  public Product(String name, String description, Money unitPrice, Category category, int quantity) {
    this.name = name;
    this.description = description;
    this.unitPrice = unitPrice;
    this.category = category;
    this.quantity = quantity;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public Money getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(Money unitPrice) {
    this.unitPrice = unitPrice;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Product product = (Product) o;

    if (quantity != product.quantity) return false;
    if (id != null ? !id.equals(product.id) : product.id != null) return false;
    if (name != null ? !name.equals(product.name) : product.name != null) return false;
    if (description != null ? !description.equals(product.description) : product.description != null) return false;
    if (unitPrice != null ? !unitPrice.equals(product.unitPrice) : product.unitPrice != null) return false;
    return !(category != null ? !category.equals(product.category) : product.category != null);

  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (unitPrice != null ? unitPrice.hashCode() : 0);
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + quantity;
    return result;
  }
}
