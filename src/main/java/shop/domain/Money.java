package shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Money implements Comparable<Money> {

  protected static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

  private static final int DEFAULT_SCALE = 2;

  public static final Money ZERO = new Money(BigDecimal.ZERO, DEFAULT_SCALE);

  private BigDecimal value;

  public Money() {
    this(BigDecimal.ZERO);
  }

  public Money(String value) {
    this(fromString(value));
  }

  public Money(BigDecimal value) {
    this(value, DEFAULT_SCALE);
  }

  protected Money(BigDecimal value, int scale) {
    this.value = value.setScale(scale, DEFAULT_ROUNDING_MODE);
  }

  public BigDecimal getValue() {
    return value;
  }

  @Override
  public int compareTo(Money other) {
    return value.compareTo(other.value);
  }

  public Money add(Money other) {
    return new Money(value.add(other.value), value.scale());
  }

  public Money subtract(Money other) {
    return new Money(value.subtract(other.value), value.scale());
  }

  public Money multiply(BigDecimal times) {
    return new Money(value.multiply(times), value.scale());
  }

  public BigDecimal divide(Money other) {
    return other.isZero() ? BigDecimal.ZERO : value.divide(other.value, 6, DEFAULT_ROUNDING_MODE);
  }

  public Money divide(BigDecimal divisor) {
    return divide(divisor, DEFAULT_ROUNDING_MODE);
  }

  public Money divide(BigDecimal divisor, RoundingMode roundingMode) {
    return (BigDecimal.ZERO.compareTo(divisor) == 0) ? ZERO : new Money(value.divide(divisor, value.scale(), roundingMode));
  }

  public boolean isZero() {
    return value.compareTo(ZERO.getValue()) == 0;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || !(o instanceof Money)) {
      return false;
    }

    Money other = (Money) o;

    return value.equals(other.value);
  }

  public boolean isNegative() {
    return value.compareTo(BigDecimal.ZERO) < 0;
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public String toString() {
    return value.toString();
  }

  protected static BigDecimal fromString(String value) {
    return new BigDecimal(value.replace(",", ""));
  }
}
