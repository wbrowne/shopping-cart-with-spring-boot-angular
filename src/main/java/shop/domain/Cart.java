package shop.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Cart implements Serializable {

  private LinkedHashMap<String, ProductOrder> products = new LinkedHashMap<>();

  public ProductOrder addToCart(ProductOrder productOrder) {

    String productId = productOrder.getProduct().getId();

    //If the item already exists in the cart, increment quantity..
    if (products.containsKey(productId)) {
      ProductOrder existingProductOrder = products.get(productId);
      int newQuantity = existingProductOrder.getQuantity() + productOrder.getQuantity();

      ProductOrder newProductOrder = new ProductOrder(productOrder.getProduct(), newQuantity);
      products.put(productId, newProductOrder);

      return newProductOrder;
    } else {
      //assuming only one product at a time..but needs validation check
      products.put(productId, productOrder);

      return productOrder;
    }
  }

  public void removeItemFromCart(String productId) {
    products.remove(productId);
  }

  public void updateCart(ProductOrder productOrder) {
    if (productOrder != null) {
      String productId = productOrder.getProduct().getId();

      if (products.containsKey(productId)) {
        if (productOrder.getQuantity() <= 0) {
          removeItemFromCart(productId);
          return;
        }
      }
      products.put(productId, productOrder);
    }
  }

  public List<ProductOrder> getProductsInCart() {
    return new ArrayList<>(products.values());
  }

  public int getNumProductsInCart() {
    int numProductsInCart = 0;
      for (ProductOrder productInCart : getProductsInCart()) {
        numProductsInCart += productInCart.getQuantity();
      }
    return numProductsInCart;
  }

  public void clearCart() {
    products.clear();
  }
}
