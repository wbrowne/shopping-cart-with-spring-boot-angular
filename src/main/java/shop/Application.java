package shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;

import shop.domain.Category;
import shop.domain.Money;
import shop.domain.Order;
import shop.domain.Product;

@SpringBootApplication
public class Application implements CommandLineRunner {

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public void run(String... strings) throws Exception {
    saveSampleData();
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  private void saveSampleData() {
    mongoTemplate.dropCollection(Category.class);
    mongoTemplate.dropCollection(Product.class);
    mongoTemplate.dropCollection(Order.class);

    Category electronics = new Category("Electronics");
    Category fashion = new Category("Fashion");
    Category books = new Category("Books");
    Category motors = new Category("Motors");

    mongoTemplate.save(electronics);
    mongoTemplate.save(fashion);
    mongoTemplate.save(books);
    mongoTemplate.save(motors);

    mongoTemplate.save(new Product("Apple Macbook Pro 13\"", "2015 Model. 8GB RAM, 2.6Ghz Intel processor", new Money("1700"), electronics));
    mongoTemplate.save(new Product("SuperDry Jacket", "Wind cheater, Double zipped, Fleece line black jacket", new Money("49.99"), fashion));
    mongoTemplate.save(new Product("Clean Code", "How to write clean code with Uncle Bob", new Money("15"), books));
    mongoTemplate.save(new Product("Ford Mustang", "2014 model in Candy Red", new Money("75000"), motors));
  }
}