package shop.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import shop.Application;
import shop.domain.Money;
import shop.domain.Product;
import shop.domain.ProductOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Application.class)
public class CartServiceImplIntegrationTest {

  @Autowired
  private CartService cartService;

  @Test
  public void testListProducts() throws Exception {
    List<ProductOrder> orderProducts = cartService.getProductsInCart();
    assertThat(orderProducts, IsEmptyCollection.empty());

    Product prod = new Product("1", "test", "test", new Money("10"), null);
    cartService.addToCart(new ProductOrder(prod, 10));
    assertThat(cartService.getProductsInCart().size(), equalTo(1));
  }
}
