package shop.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static shop.util.TestUtil.sampleProductOrder;
import static shop.util.TestUtil.sampleProductOrderList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import shop.domain.Cart;
import shop.domain.Money;
import shop.domain.Order;
import shop.domain.Product;
import shop.domain.ProductOrder;
import shop.util.TestUtil;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceImplTest {

  @Mock
  private Cart cart;

  private CartServiceImpl cartService;

  @Before
  public void setup() throws Exception {
    cartService = new CartServiceImpl();

    cartService.setCart(cart);
  }

  @Test
  public void testAddAndFetchProductsInCart() {
    ProductOrder productOrder = sampleProductOrder();

    stubAddProductOrderToCart(productOrder);
    stubCartToReturnProductOrders(new ArrayList<>(Collections.singletonList(productOrder)));

    ProductOrder productInCart = cartService.addToCart(productOrder);

    assertEquals(cartService.getProductsInCart().size(), 1);
    assertEquals(cartService.getProductsInCart().get(0), productOrder);
    assertEquals(productInCart, productOrder);
  }

  @Test
  public void testSubmitOrderWithNoProductOrders() {

    when(cart.getProductsInCart()).thenReturn(Collections.<ProductOrder>emptyList());

    Order order = cartService.submitOrder();
    assertNull(order);

    verify(cart, times(1)).getProductsInCart();
    verify(cart, never()).clearCart();
  }

  @Test
  public void testSubmitOrderWithProductOrders() {

    when(cart.getProductsInCart()).thenReturn(sampleProductOrderList());

    Order order = cartService.submitOrder();
    assertNotNull(order);
    assertEquals(sampleProductOrderList(), order.getProductOrders());

    verify(cart, times(1)).getProductsInCart();
    verify(cart, times(1)).clearCart();
  }

  private void stubAddProductOrderToCart(ProductOrder productOrder) {
    when(cart.addToCart(productOrder)).thenReturn(productOrder);
  }

  private void stubCartToReturnProductOrders(List<ProductOrder> productOrders) {
    when(cart.getProductsInCart()).thenReturn(productOrders);
  }

}
