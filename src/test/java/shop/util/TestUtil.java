package shop.util;

import java.util.ArrayList;
import java.util.List;

import shop.domain.Money;
import shop.domain.Order;
import shop.domain.Product;
import shop.domain.ProductOrder;

public class TestUtil {
  public static List<ProductOrder> sampleProductOrderList() {
    List<ProductOrder> productOrders = new ArrayList<>();

    Product product1 = new Product("1", "test 1", "test", new Money("10"), null);
    Product product2 = new Product("2", "test 2", "test", new Money("4.99"), null);

    ProductOrder productOrder1 = new ProductOrder(product1, 3);
    ProductOrder productOrder2 = new ProductOrder(product2, 2);

    productOrders.add(productOrder1);
    productOrders.add(productOrder2);

    return productOrders;
  }

  public static ProductOrder sampleProductOrder() {
    Product product = new Product("1", "test 1", "test", new Money("10"), null);

    return new ProductOrder(product, 1);
  }

  public static Order sampleOrder() {
    return new Order(sampleProductOrderList());
  }
}
