package shop.domain;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class CartTest {

  private Map<String, Product> testProducts = new HashMap<>();

  private Cart cart;

  @Before
  public void setUp() {
    cart = new Cart();

    testProducts.put("1", new Product("1", "Product 1", "Product 1 desc", new Money("1"), null));
    testProducts.put("2", new Product("2", "Product 2", "Product 2 desc", new Money("2"), null));
    testProducts.put("3", new Product("3", "Product 3", "Product 3 desc", new Money("3"), null));
    testProducts.put("4", new Product("4", "Product 4", "Product 4 desc", new Money("4"), null));
  }

  @Test
  public void testAddToCart() {
    ProductOrder productOrder = new ProductOrder(testProducts.get("1"), 2);

    cart.addToCart(productOrder);

    List<ProductOrder> productsInCart = cart.getProductsInCart();

    assertEquals(productsInCart.size(), 1);
    assertEquals(productsInCart.get(0).getProduct(), productOrder.getProduct());
    assertEquals(productsInCart.get(0).getQuantity(), productOrder.getQuantity());
  }

  @Test
  public void testAddToCartWithExistingProductOrder() {
    ProductOrder productOrder1 = new ProductOrder(testProducts.get("1"), 1);
    cart.addToCart(productOrder1);

    ProductOrder productOrder2 = new ProductOrder(testProducts.get("1"), 1);
    cart.addToCart(productOrder2);

    assertEquals(cart.getProductsInCart().size(), 1);
    assertEquals(cart.getProductsInCart().get(0).getProduct(), productOrder2.getProduct());
    assertEquals(cart.getProductsInCart().get(0).getQuantity(), productOrder1.getQuantity() + productOrder2.getQuantity());
  }

  @Test
  public void testUpdateCart() {
    Product testProduct = testProducts.get("1");

    ProductOrder productOrder1 = new ProductOrder(testProduct, 3);
    cart.addToCart(productOrder1);

    ProductOrder productOrderUpdate = new ProductOrder(testProduct, 4);
    cart.updateCart(productOrderUpdate);

    assertEquals(cart.getProductsInCart().size(), 1);
    assertEquals(cart.getProductsInCart().get(0).getProduct(), productOrderUpdate.getProduct());
    assertEquals(cart.getProductsInCart().get(0).getQuantity(), productOrderUpdate.getQuantity());
  }


  @Test
  public void testUpdateWith0QuantityInCart() {
    ProductOrder productOrder1 = new ProductOrder(testProducts.get("1"), 5);
    ProductOrder productOrder2 = new ProductOrder(testProducts.get("2"), 6);

    cart.updateCart(productOrder1);
    cart.updateCart(productOrder2);

    ProductOrder productOrder3 = new ProductOrder(testProducts.get("2"), 0);

    cart.updateCart(productOrder3);

    assertEquals(cart.getProductsInCart().size(), 1);
    assertEquals(cart.getProductsInCart().get(0).getProduct(), productOrder1.getProduct());
    assertEquals(cart.getProductsInCart().get(0).getQuantity(), productOrder1.getQuantity());
    assertThat(cart.getProductsInCart(), not(contains(productOrder2)));
  }

}
