package shop.domain;

import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class MoneyTest {

  @Test
  public void testToString() {
    assertEquals("0.00", Money.ZERO.toString());
    assertEquals("0.00", new Money("0.00").toString());
    assertEquals("-0.01", new Money( "-0.01").toString());
    assertEquals("-0.01", new Money(new BigDecimal("-.0123")).toString());
  }

  @Test
  public void testRoundingToTwoDigits() {
    assertEquals(new BigDecimal("0.23"), new Money("0.230").getValue());
    assertEquals(new BigDecimal("0.23"), new Money("0.225").getValue());
    assertEquals(new BigDecimal("0.23"), new Money("0.234").getValue());
  }

  @Test
  public void testZero() {
    assertTrue(new Money(ZERO).isZero());
    assertTrue(new Money("0").isZero());
    assertTrue(new Money("0.00").isZero());
    assertFalse(new Money("0.01").isZero());
  }

  @Test
  public void testFractions() {
    assertEquals(new BigDecimal("1.24"), new Money("1.235").getValue());
    assertEquals(new BigDecimal("1.24"), new Money("1.244").getValue());
    assertEquals(new BigDecimal("4.07"), new Money("5").divide(new BigDecimal("1.23")).getValue());
  }
}