package shop.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static shop.util.TestUtil.sampleOrder;
import static shop.util.TestUtil.sampleProductOrder;
import static shop.util.TestUtil.sampleProductOrderList;

import com.google.gson.Gson;

import shop.domain.Money;
import shop.domain.Order;
import shop.domain.Product;
import shop.domain.ProductOrder;
import shop.service.CartService;
import shop.util.TestUtil;

public class CartControllerTest {

  @Mock
  private CartService cartService;

  @Mock
  private MongoTemplate mongoTemplate;

  @InjectMocks
  private CartController cartController;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.standaloneSetup(cartController).build();
  }

  @Test
  public void testListProductOrders() throws Exception {

    when(cartService.getProductsInCart()).thenReturn(sampleProductOrderList());

    this.mockMvc.perform(get("/cart"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].product.id", is("1")))
        .andExpect(jsonPath("$[0].product.name", is("test 1")))
        .andExpect(jsonPath("$[0].product.description", is("test")))
        .andExpect(jsonPath("$[0].product.unitPrice.value", is(10.00)))
        .andExpect(jsonPath("$[0].product.category", is(nullValue())))
        .andExpect(jsonPath("$[0].quantity", is(3)))
        .andExpect(jsonPath("$[1].product.id", is("2")))
        .andExpect(jsonPath("$[1].product.name", is("test 2")))
        .andExpect(jsonPath("$[1].product.description", is("test")))
        .andExpect(jsonPath("$[1].product.unitPrice.value", is(4.99)))
        .andExpect(jsonPath("$[1].product.category", is(nullValue())))
        .andExpect(jsonPath("$[1].quantity", is(2)));

    verify(cartService, times(1)).getProductsInCart();
    verifyNoMoreInteractions(cartService);
  }

  @Test
  public void testAddToCart() throws Exception {
    ProductOrder productOrder = sampleProductOrder();

    Query query = new Query(where("id").is(productOrder.getProduct().getId()));

    when(mongoTemplate.findOne(query, Product.class)).thenReturn(productOrder.getProduct());
    when(cartService.addToCart(productOrder)).thenReturn(sampleProductOrder());

    Gson gson = new Gson();
    String json = gson.toJson(productOrder);

    this.mockMvc.perform(post("/cart")
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.product.id", is("1")))
        .andExpect(jsonPath("$product.name", is("test 1")))
        .andExpect(jsonPath("$product.description", is("test")))
        .andExpect(jsonPath("$product.unitPrice.value", is(10.00)))
        .andExpect(jsonPath("$product.category", is(nullValue())))
        .andExpect(jsonPath("$quantity", is(1)));

    verify(mongoTemplate, times(1)).findOne(query, Product.class);
    verify(cartService, times(1)).addToCart(productOrder);
    verifyNoMoreInteractions(mongoTemplate, cartService);
  }

  @Test
  public void testUpdateCart() throws Exception {
    ProductOrder productOrder = sampleProductOrder();

    Query query = new Query(where("id").is(productOrder.getProduct().getId()));
    when(mongoTemplate.findOne(query, Product.class)).thenReturn(productOrder.getProduct());
    when(cartService.getProductsInCart()).thenReturn(Collections.singletonList(productOrder));

    Gson gson = new Gson();
    String json = gson.toJson(productOrder);

    MvcResult result = mockMvc.perform(post("/updateCart")
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].product.id", is("1")))
        .andExpect(jsonPath("$[0].product.name", is("test 1")))
        .andExpect(jsonPath("$[0].product.description", is("test")))
        .andExpect(jsonPath("$[0].product.unitPrice.value", is(10.00)))
        .andExpect(jsonPath("$[0].product.category", is(nullValue())))
        .andExpect(jsonPath("$[0].quantity", is(1)))
        .andReturn();

    verify(mongoTemplate, times(1)).findOne(query, Product.class);
    verify(cartService, times(1)).updateCart(productOrder);
    verify(cartService, times(1)).getProductsInCart();
    verifyNoMoreInteractions(mongoTemplate, cartService);
  }

  @Test
  public void testDeleteFromCart() throws Exception {
    ProductOrder productOrder = sampleProductOrder();

    Query query = new Query(where("id").is(productOrder.getProduct().getId()));

    when(mongoTemplate.findOne(query, Product.class)).thenReturn(productOrder.getProduct());
    when(cartService.getProductsInCart()).thenReturn(Collections.singletonList(sampleProductOrder()));

    Gson gson = new Gson();
    String json = gson.toJson(productOrder);

    this.mockMvc.perform(post("/deleteCart")
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].product.id", is("1")))
        .andExpect(jsonPath("$[0].product.name", is("test 1")))
        .andExpect(jsonPath("$[0].product.description", is("test")))
        .andExpect(jsonPath("$[0].product.unitPrice.value", is(10.00)))
        .andExpect(jsonPath("$[0].product.category", is(nullValue())))
        .andExpect(jsonPath("$[0].quantity", is(1)));

    verify(mongoTemplate, times(1)).findOne(query, Product.class);
    verify(cartService, times(1)).removeFromCart(productOrder.getProduct().getId());
    verify(cartService, times(1)).getProductsInCart();
    verifyNoMoreInteractions(mongoTemplate, cartService);
  }

  @Test
  public void testGetProductOrderCount() throws Exception {

    when(cartService.getNumberOfProductsInCart()).thenReturn(3);

    this.mockMvc.perform(get("/cartCount"))
        .andExpect(status().isOk())
        .andExpect(content().string("3"));

    verify(cartService, times(1)).getNumberOfProductsInCart();
    verifyNoMoreInteractions(cartService);
  }

  @Test
  public void testSubmitOrder() throws Exception {
    when(cartService.submitOrder()).thenReturn(sampleOrder());
    when(cartService.getProductsInCart()).thenReturn(Collections.singletonList(sampleProductOrder()));

    this.mockMvc.perform(post("/orders"))
        .andExpect(status().isOk());

    verify(cartService, times(1)).submitOrder();
    verify(mongoTemplate, times(1)).insert(sampleOrder());
    verify(cartService, times(1)).getProductsInCart();
    verifyNoMoreInteractions(cartService, mongoTemplate);
  }

}
